package com.example.test

data class ContactModel(val contactName: String, val phoneNumber: String, val imageUri: Int)

