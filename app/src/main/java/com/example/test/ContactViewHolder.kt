package com.example.test

import android.widget.AdapterView
import androidx.recyclerview.widget.RecyclerView
import com.example.test.databinding.ItemContactBinding

class ContactViewHolder(
     binding: ItemContactBinding
): RecyclerView.ViewHolder(binding.root) {
    fun bindView(contacts: ContactModel, listener: RecyclerViewAdapter.OnItemClickListener) {
        itemView.setOnClickListener { listener.onItemClick(contacts) }
    }

    val name = binding.contactName
    val number = binding.contactNumber
    val image = binding.contactImage
}
