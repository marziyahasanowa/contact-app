package com.example.test

import android.annotation.SuppressLint
import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.provider.ContactsContract
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ListView
import android.widget.SimpleCursorAdapter
import androidx.fragment.app.Fragment

class ListViewFragment : Fragment(), ContactFetcher {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_list_view, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fetchContacts()
    }

    @SuppressLint("Range")
    override fun fetchContacts() {
        val cursor: Cursor? = requireActivity().contentResolver.query(
            ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null
        )
        val from = arrayOf(
            ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
            ContactsContract.CommonDataKinds.Phone.NUMBER,
            ContactsContract.CommonDataKinds.Phone.PHOTO_URI
        )

        val to = intArrayOf(R.id.contact_name, R.id.contact_number, R.id.contact_image)

        val simple = SimpleCursorAdapter(
            requireContext(), R.layout.item_contact, cursor, from, to, 0
        )

        val listView = view?.findViewById<ListView>(R.id.listView)
        if (listView != null) {
            listView.adapter = simple
        }

        listView?.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                cursor?.moveToPosition(position)
                val phoneNumber =
                    cursor?.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                if (!phoneNumber.isNullOrEmpty()) {
                    val dialIntent = Intent(Intent.ACTION_DIAL)
                    dialIntent.data = Uri.parse("tel:$phoneNumber")
                    startActivity(dialIntent)
                }
            }
    }
}


