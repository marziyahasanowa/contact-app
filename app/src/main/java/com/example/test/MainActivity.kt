package com.example.test

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.example.test.databinding.ActivityMainBinding
import com.google.android.material.navigation.NavigationView


class MainActivity : AppCompatActivity() {

    private val READ_CONTACTS_PERMISSION_CODE = 1001
    private lateinit var navView: NavigationView
    private lateinit var drawerLayout: DrawerLayout
    lateinit var toggle: ActionBarDrawerToggle
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        //Setting up the drawer nav
        navView = findViewById(R.id.nav_header)
        drawerLayout = findViewById(R.id.drawer_layout)
        toggle = ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close)
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        navView.setNavigationItemSelectedListener {
            when(it.itemId){
                R.id.list_view -> {
                    Log.d("list fetch",  "list fetch")
                    listViewFragmentAttach()
                }
                R.id.recycler_view -> {
                    Log.d("recycle fetch",  "recycle fetch")
                    recyclerViewFragmentAttach()
                }
            }
            drawerLayout.closeDrawer(GravityCompat.START)
            true
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (toggle.onOptionsItemSelected(item)){
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    //fragment transactions on option click
    private fun listViewFragmentAttach() {
        val listViewFragmentTransaction = supportFragmentManager.beginTransaction()
        listViewFragmentTransaction.replace(R.id.fragment_container, ListViewFragment())
        listViewFragmentTransaction.commit()
    }

    private fun recyclerViewFragmentAttach() {
        val recyclerViewFragmentTransaction = supportFragmentManager.beginTransaction()
        recyclerViewFragmentTransaction.replace(R.id.fragment_container, RecyclerViewFragment())
        recyclerViewFragmentTransaction.commit()
    }
}


interface ContactFetcher {
    fun fetchContacts()
}
