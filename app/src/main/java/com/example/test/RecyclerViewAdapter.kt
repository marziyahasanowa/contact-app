package com.example.test

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.test.databinding.ItemContactBinding


class RecyclerViewAdapter(
    private var contactList:MutableList<ContactModel>,
    private var listener: OnItemClickListener
) : RecyclerView.Adapter<ContactViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactViewHolder {
        Log.d("onCreateViewHolder", "onCreateViewHolder")
        return ContactViewHolder(ItemContactBinding.inflate(LayoutInflater.from(parent.context),parent, false))
    }

    override fun onBindViewHolder(holder: ContactViewHolder, position: Int) {
        Log.d("onBindViewHolder", "onBindViewHolder")
        val item = contactList[position]
        holder.bindView(item, listener)
        holder.name.text = item.contactName
        holder.number.text = item.phoneNumber
        Glide.with(holder.itemView)
            .load(item.imageUri)
            .placeholder(R.drawable.baseline_person_24)
            .into(holder.image)
    }

    override fun getItemCount(): Int {
        Log.d("getItemCount", "getItemCount")
        return contactList.size
    }

    interface OnItemClickListener {
        fun onItemClick(contacts: ContactModel)
    }
}