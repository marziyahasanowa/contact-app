package com.example.test

import android.content.pm.PackageManager
import android.os.Bundle
import android.provider.ContactsContract
import androidx.fragment.app.Fragment
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.test.databinding.FragmentRecyclerViewBinding
import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.util.Log

class RecyclerViewFragment : Fragment(R.layout.fragment_recycler_view), ContactFetcher, RecyclerViewAdapter.OnItemClickListener{
    private lateinit var binding: FragmentRecyclerViewBinding
    private lateinit var recyclerView: RecyclerView

    private val contactList = mutableListOf<ContactModel>()
    private val adapter = RecyclerViewAdapter(contactList, this)



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentRecyclerViewBinding.bind(view)
        recyclerView = binding.recyclerView
        recyclerView.adapter = adapter
        fetchContacts()
    }

    @SuppressLint("Range")
    override fun fetchContacts() {
        Log.d("fetch contacts", "fetching")
        val cursor = requireActivity().contentResolver.query(
            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
            null,
            null,
            null,
            null
        )

        cursor?.use {
            while (it.moveToNext()) {
                val name =
                    it.getString(it.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME))
                val number =
                    it.getString(it.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))

                val image =
                    it.getInt(it.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI))

                val contact = ContactModel(name, number, image)
                contactList.add(contact)
            }
        }
        cursor?.close()
        // Notify the adapter that the data has changed
        adapter.notifyDataSetChanged()
    }

    override fun onItemClick(contact: ContactModel) {
        val phoneNumber = contact.phoneNumber
        if (!phoneNumber.isNullOrEmpty()) {
            val dialIntent = Intent(Intent.ACTION_DIAL)
            dialIntent.data = Uri.parse("tel:$phoneNumber")
            startActivity(dialIntent)
        }
    }
}